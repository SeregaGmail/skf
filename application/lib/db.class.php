<?php

namespace skf;

/**
 * DatabaseFactory
 * Creates an DataBaseBindings object for use from the application layer
 * @depends    PostgreSQL, MySQL
 */

class db
{
	/**
	 * factory
	 *
	 * Sets an DataBaseBinding compatible object based on the type of database
	 * defined in $config_values['database']['db_type']
	 */

	public $conn;

	public function __construct()
	{
		$config = new config; 
		$db_type = strtolower( $config->config_values['database']['db_type'] );

		switch( $db_type )
		{
			case 'pgsql':
			case 'postgresql':
			case 'mysql':
				$config		= new config;
				$db_type	= $config->config_values['database']['db_type'];
				$hostname	= $config->config_values['database']['db_hostname'];
				$dbname		= $config->config_values['database']['db_name'];
				$db_password	= $config->config_values['database']['db_password'];
				$db_username	= $config->config_values['database']['db_username'];
				$db_port	= $config->config_values['database']['db_port'];

				$this->conn = new \PDO( "$db_type:host=$hostname;port=$db_port;dbname=$dbname", $db_username, $db_password );
				$this->conn-> setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
			break;

			case 'sqlite':
				try{
				$path = APP_PATH . $config->config_values['database']['db_path'].'/'.$config->config_values['database']['db_name'].'.sq3';
				$this->conn = new \PDO( "sqlite:$path" );
				$this->conn-> setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
				}
				catch( \PDOException $e )
				{
					echo $path;
				}
			break;

			default:
			throw new Exception('Database type not supported');
		}
	}
}
?>
