<?php
/**
 * File containing the admin controller
 *
 * @package SKF
 * @copyright Copyright (C) 2009 PHPRO.ORG. All rights reserved.
 *
 */

namespace skf;

class adminController extends baseController implements IController
{

	public function __construct()
	{
		parent::__construct();
		// a new config
		$config = new config;
		$this->view->version = $config->config_values['application']['version'];
	}

	public function index()
	{
		/*** a new view instance ***/
		$tpl = new view;

		/*** turn caching on for this page ***/
		$tpl->setCaching(true);

		/*** set the template dir ***/
		$tpl->setTemplateDir(APP_PATH . '/modules/admin/views');

		/*** the include template ***/
		// $tpl->include_tpl = APP_PATH . '/views/admin/index.phtml';

		/*** a view variable ***/
		$this->view->title = 'Admin';

		/*** the cache id is based on the file name ***/
		$cache_id = md5( 'admin/index.phtml' );

		/*** fetch the template ***/
		$this->content = $tpl->fetch( 'index.phtml', $cache_id);
	}

        public function buildDao()
        {
                $this->view->body_id = 'admin';

                /*** a new view instance ***/
                $tpl = new view;

                /*** turn caching on for this page ***/
                // $view->setCaching(true);

                /*** set the template dir ***/
                $tpl->setTemplateDir( APP_PATH.'/modules/admin/views' );

                /*** a view variable ***/
                $this->view->title = 'Admin';

                // a new config
                $config = new config;
                $this->view->version =
		$config->config_values['application']['version'];

                $tpl->table_name = '';
                $tpl->table_name_error = '';

                $tpl->post_status = 'info';
                $message = 'Add or Delete Banner from the home page main Slider';

                try
                {
                        if( isset( $_POST['table_name'] ) && $_POST['table_name'] == '' )
                        {
                                $db = new \skf\db;
                                // form posted without table name, renew ALL
                                // dao objects
                                $dao = new \skf\dao( $db->conn );
                                $dao->generateDAO();
                                $message = 'All DAOs successfully updated';
                                $tpl->post_status = 'success';
                        }

                        if( isset( $_POST['table_name'] ) && $_POST['table_name'] != '' )
                        {
                                $db = new \skf\db;
                                // form post with table name, rebuild this
                                // table only
                                $table_name = $_POST['table_name'];
                                $dao = new \skf\mysql_dao( $db->conn, null );
                                $dao->generateDAO();
                                $message = "DAO for Table '$table_name' successfully updated";
                                $tpl->post_status = 'success';
                        }
                }
                catch( Exception $e )
                {
                        $tpl->post_status = 'error';
                        $message = 'Could not rebuild DAO: '.$e->getMessage();
                }
                $tpl->message = $message;
                /*** the cache id is based on the file name ***/
                $cache_id = md5( 'admin/builddao.php' );

                /*** fetch the template ***/
                $this->content = $tpl->fetch( 'builddao.phtml', $cache_id);
        }
}
